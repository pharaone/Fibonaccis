//
//  main.c
//  Fibonaccis 0.1.2
//
//  Created by Emanuele Antonio Faraone on 23/09/17.
//  Last modified by Emanuele Antonio Faraone on 13/11/2018.
//  Copyright © 2017-2018 Emanuele Antonio Faraone. All rights reserved.
//
#include <stdio.h>
int main() {
	int prec1=1; // first number
    int prec2=0; // second number
    int r,i,howmanyt  = 0 ; // r: result, howmanyt : how many times to do the fibonacci sequence
    FILE *fd;   //now you are able to write the sequence on a file
    fd = fopen(".//fibonacci.txt","w+");
    printf("How many times do I have to do the Fibonacci's sequence?\n");
    scanf("%d" ,&howmanyt);
    if (howmanyt < 1){
    	printf("You can`t set 0 or a negative number, please write a new number: ");
    	scanf("%d",&howmanyt);
	}else if (howmanyt > 45){
		printf("For now Fibonaccis doesn`t support doing the sequence more than 45 times, please write a new number: ");
    	scanf("%d",&howmanyt);
	}
    for (i=0; i<=howmanyt;i++){
        r=prec1+prec2;
        printf("\n" "%d", r);
        prec1=prec2 ;
        prec2=r;
        fprintf(fd,"%d \n", r);
    }
    
    printf("\n Fibonaccis ran %d times the Fibonacci series,", howmanyt);
    printf(" the last number of the sequence was %d \n", r);
    fprintf(fd,"\n Fibonaccis ran %d times the Fibonacci series,,", howmanyt);
    fprintf(fd," the last number of the sequence was %d \n", r);
    fclose(fd);
}
